//
//  EQLeftPanelViewController.h
//  EarthQuakism
//
//  Created by Yoon Lee on 7/12/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

extern const CGFloat EQLeftDefaultWidth;

@interface EQLeftPanelViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@end
