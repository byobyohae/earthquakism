//
//  EQLeftPanelViewController.m
//  EarthQuakism
//
//  Created by Yoon Lee on 7/12/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "EQLeftPanelViewController.h"

const CGFloat EQLeftDefaultWidth  = 155.f;

@interface EQLeftPanelViewController ()
@property(nonatomic, retain)UITableView *tableView;
@property(nonatomic, retain)NSArray *contents, *components;
@end

@implementation EQLeftPanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:CWHITE()];
    
    CGRect frame = self.view.bounds;
    frame.size.width = EQLeftDefaultWidth;
    
    /* UITableView setting */
    _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setRowHeight:55.f];
    [self.tableView setBackgroundColor:CCLEAR()];
    
    frame.size.height = 44.f;
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:frame];
    [self.tableView setTableHeaderView:tableHeaderView];
    [self.view addSubview:self.tableView];
    
    /* Dataset setting */
    _contents   = @[@"Profile", @"Locks", @"Settings", @"Logout", @"About"];
    _components = @[@"", @"", @"EQApp Configurations", @"Logs out", @"EQApp References"];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIFont *font = [UIFont fontWithName:@"Avenir-Medium" size:14.f];
    NSString *titleStr = [self.contents objectAtIndex:indexPath.row];
    [cell.textLabel setText:titleStr];
    [cell.textLabel setFont:font];
    
    UIFont *detailFont = [UIFont fontWithName:@"Avenir-Medium" size:12.f];
    NSString *detailStr = [self.components objectAtIndex:indexPath.row];
    [cell.detailTextLabel setText:detailStr];
    [cell.detailTextLabel setNumberOfLines:0.f];
    [cell.detailTextLabel setTextColor:CDGREY()];
    [cell.detailTextLabel setFont:detailFont];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CONFIGURATIONCELL_IDENTIFIER = @"CONFIGURATIONCELL_IDENTIFIER";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CONFIGURATIONCELL_IDENTIFIER];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CONFIGURATIONCELL_IDENTIFIER];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
