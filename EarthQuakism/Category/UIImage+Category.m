//
//  UIImage+Category.m
//  BlackCloset
//
//  Created by Yoon Lee on 5/7/14.
//  Copyright (c) 2014 BrandBoom. All rights reserved.
//

#import "UIImage+Category.h"

@implementation UIImage(Colored)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
