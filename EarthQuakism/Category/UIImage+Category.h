//
//  UIImage+Category.h
//  BlackCloset
//
//  Created by Yoon Lee on 5/7/14.
//  Copyright (c) 2014 BrandBoom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(Extentions)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
