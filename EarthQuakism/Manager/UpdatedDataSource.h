//
//  UpdatedDataSource.h
//  EarthQuakism
//
//  Created by Yoon Lee on 4/14/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdatedDataSource : NSObject

@property(nonatomic, strong)NSMutableArray *mergedBySameCity;

+ (id)sharedUpdateDataSource;

- (NSArray *)getEntireEQRegions;
- (NSArray*)getGroup:(NSInteger)index;

@end
