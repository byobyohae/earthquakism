//
//  EQQueryManager.m
//  EarthQuakism
//
//  Created by Yoon Lee on 3/30/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//
// endtime=2014-04-01 23:59:59
#import "EQQueryManager.h"
#define EARTHQUAKE_DEFAULT_SEARCH_URI   @"http://comcat.cr.usgs.gov/fdsnws/event/1/query"
#define EARTHQUAKE_SAMEPLE_QUERY        @"?minmagnitude=3&eventtype=earthquake&format=geojson&maxmagnitude=9&orderby=time"

@interface EQQueryManager()

// mutable string has better performance on appending with other string
@property(nonatomic, strong)NSMutableString *query;
// i realized that message was sent to device by packet, and sometimes sealed by multiple its pieces. so that had to collect puzzled data.
@property(nonatomic, strong)NSMutableData *collection;

@end

@implementation EQQueryManager
@synthesize query;
@synthesize collection;

- (void)shakeEQ:(NSString*)keyword
{
    [self setQuery:[[NSMutableString alloc] initWithString:EARTHQUAKE_DEFAULT_SEARCH_URI]];
    // sending with parameters with keyword
    NSString *now = [[self getDateNow] objectForKey:@"today"];
    NSString *yesterday = [[self getDateNow] objectForKey:@"yesterday"];
    [self.query appendFormat:@"%@&starttime=%@&endtime=%@", EARTHQUAKE_SAMEPLE_QUERY, yesterday, now];
}

- (void)executeQuery
{
    collection = [[NSMutableData alloc] init];
    NSURL *uri = [NSURL URLWithString:[self.query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSURLRequest *request = [NSURLRequest requestWithURL:uri cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.f];
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self startImmediately:YES];
    [connection start];
}

#pragma mark NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // collecting data piece into one
    [collection appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *err = nil;
    id json = [collection objectFromJSONDataWithParseOptions:JKParseOptionNone error:&err];
    if (err) {
        DLog(@"%@", [err description]);
    }
    else {
        [_delegate didReceivedResponseFromQueryManager:self withResponse:json];
    }
    
    collection = nil;
}

- (NSDictionary*)getDateNow
{
    // grab a time date right now.
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:-1];
    NSDate *yesterday = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    // format the date
    NSString *formToday = @"";
    NSString *formYesterday = @"";
    // assign date from string using formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // converts the date format as we wish
    [dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:00"];
    formYesterday = [dateFormatter stringFromDate:yesterday];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    formToday = [dateFormatter stringFromDate:[NSDate date]];
    
    return @{@"today": formToday, @"yesterday": formYesterday};
}

@end
