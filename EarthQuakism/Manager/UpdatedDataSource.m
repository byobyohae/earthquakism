//
//  UpdatedDataSource.m
//  EarthQuakism
//
//  Created by Yoon Lee on 4/14/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "UpdatedDataSource.h"

@implementation UpdatedDataSource

+ (id)sharedUpdateDataSource
{
    static UpdatedDataSource *datasource = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        datasource = [[UpdatedDataSource alloc] init];
    });
    
    return datasource;
}

- (NSArray *)getEntireEQRegions
{
    NSArray *EQRegions = nil;
    
    if (self.mergedBySameCity != nil) {
        EQRegions = [NSArray arrayWithArray:self.mergedBySameCity];
    }
    
    return EQRegions;
}

- (NSArray *)getGroup:(NSInteger)index
{
    NSArray *group = [self.mergedBySameCity objectAtIndex:index];
    return group;
}

@end
