//
//  EQQueryManager.h
//  EarthQuakism
//
//  Created by Yoon Lee on 3/30/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
// borrowed fastest JSON serializer class from github
#import "JSONKit.h"

@protocol QueryManagerDelegate;

@interface EQQueryManager : NSObject<NSURLConnectionDataDelegate>

@property(nonatomic, assign)id<QueryManagerDelegate> delegate;

- (void)shakeEQ:(NSString*)keyword;
// excuting the query
- (void)executeQuery;
- (NSDictionary*)getDateNow;

@end

@protocol QueryManagerDelegate
- (void)didReceivedResponseFromQueryManager:(EQQueryManager*)qm withResponse:(id)response;
@end
