//
//  EQCityDetailViewController.m
//  EarthQuakism
//
//  Created by Yoon Lee on 4/14/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "EQCityDetailViewController.h"
#import "TLSpringFlowLayout.h"
#import "CityDetailCollectionViewCell.h"
#import "CityDetailCollectionCollectionHeaderView.h"
#import "EQMapViewController.h"

enum {
    NAME_OF_PLACE = 0,
    MAP_OF_PLACE,
    EARTHQUAKE_PATH,
}ROW_IDENTITY;

@interface EQCityDetailViewController();
@property(nonatomic, strong)NSArray *detailTitle;

@property(nonatomic, strong)EQMapViewController *mapVC;
@property(nonatomic, strong)UIBarButtonItem *rightBarButtonItem;
@property(nonatomic, strong)ADBannerView *adView;

- (void)rightBarButtonClicked:(UIBarButtonItem *)rightBarButton;

@end
static NSString *CellIdentifier = @"CellIdentifier";
static NSString *HeaderCellIdentifier = @"HeaderCellIdentifier";

@implementation EQCityDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_collectionView setBackgroundColor:CWHITE()];
    [_collectionView registerClass:[CityDetailCollectionViewCell class] forCellWithReuseIdentifier:CellIdentifier];
    [_collectionView registerClass:[CityDetailCollectionCollectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HeaderCellIdentifier];
    [_collectionView setAlwaysBounceVertical:YES];
    
    [self setTitle:@"Information"];
    
    /* map view controller */
    _mapVC = [[EQMapViewController alloc] init];
    
    /* group settings in MapViewController */
    [self.mapVC setGroup:[[UpdatedDataSource sharedUpdateDataSource] getGroup:self.selectedCity.row]];
    [self addChildViewController:self.mapVC];
    
    /* close right bar item */
    _rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(rightBarButtonClicked:)];
    [self.navigationItem setRightBarButtonItem:self.rightBarButtonItem];
    [self.rightBarButtonItem setEnabled:NO];
    
    _detailTitle = @[@"Origin of Earthquakes", @"Maps", @"Today in Earthquake History", @"Social Network"];
    
    /* advertising! let's fired up! */
    _adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    [self.adView setDelegate:self];
    [self.view addSubview:self.adView];
    
    /* GoogleAnalytics: records class name */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:NSStringFromClass([self class])];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect frame = self.adView.frame;
    frame.origin.y = self.view.frame.size.height - self.adView.frame.size.height;
    [self.adView setFrame:frame];
}

- (void)rightBarButtonClicked:(UIBarButtonItem *)rightBarButton
{
    NSLog(@".closing tap!");
    
    [UIView animateWithDuration:0.f animations:^{
        [self.collectionView performBatchUpdates:^{
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:MAP_OF_PLACE]]];
        } completion:^(BOOL finished){
            [self.rightBarButtonItem setEnabled:NO];
            [self.collectionView setScrollEnabled:YES];
        }];
    }];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 2;
    switch (section) {
        case NAME_OF_PLACE:
        case MAP_OF_PLACE:
        case EARTHQUAKE_PATH:
            numberOfRows = 1;
            break;
        default:
            break;
    }
    return numberOfRows;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CityDetailCollectionCollectionHeaderView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HeaderCellIdentifier forIndexPath:indexPath];
    [reusableview setTitle:[_detailTitle objectAtIndex:indexPath.section]];
    
    return reusableview;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    CGRect frame = cell.contentView.bounds;
    
    if (indexPath.section == MAP_OF_PLACE) {
        CGRect mapDisplayFrame = CGRectMake(0, 0, frame.size.width, 120.f);
        [self.mapVC.view setFrame:mapDisplayFrame];
        
        /* setting center of view in MapViewController */
        CGPoint centre = self.mapVC.view.center;
        centre.x = frame.size.width/2;
        centre.y = frame.size.height/2;
        [self.mapVC.view setCenter:centre];
        
        /* setting center of mapview in MapViewController */
        mapDisplayFrame = self.mapVC.view.frame;
        mapDisplayFrame.origin.x = 0.f;
        mapDisplayFrame.origin.y = 0.f;
        [self.mapVC.mapView setFrame:mapDisplayFrame];
        
        NSArray *coordinate = [[[[[UpdatedDataSource sharedUpdateDataSource] getGroup:self.selectedCity.row] objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"coordinates"];
        CLLocationDegrees latitude  = [[coordinate objectAtIndex:1] doubleValue];
        CLLocationDegrees longitude = [[coordinate objectAtIndex:0] doubleValue];
        CLLocationCoordinate2D coordinate2D = CLLocationCoordinate2DMake(latitude, longitude);
        [self.mapVC.mapView setRegion:MKCoordinateRegionMake(coordinate2D, MKCoordinateSpanMake(2.25, 2.25)) animated:YES];
        [cell.contentView addSubview:self.mapVC.mapView];
    }
    else if (indexPath.section == NAME_OF_PLACE) {
        NSString *place = [[[self.mapVC.group objectAtIndex:0] objectForKey:@"properties"] objectForKey:@"place"];
        NSArray *originInfo = [place componentsSeparatedByString:@" of "];
        if (originInfo.count > 1)
            place = [originInfo objectAtIndex:1];
        UILabel *textLabel = [(CityDetailCollectionViewCell*)cell textLabel];
        [textLabel setText:place];
    }
    else if (indexPath.section == EARTHQUAKE_PATH) {
        [self.mapVC.group enumerateObjectsUsingBlock:^(NSDictionary *info, NSUInteger idx, BOOL *stop){
            UIFont *font = [UIFont fontWithName:@"Avenir-Medium" size:15];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 17 * idx + 5, frame.size.width, 16)];
            NSString *time = [self getTimeInformation:[[[info objectForKey:@"properties"] objectForKey:@"time"] doubleValue]];
            float magnitude = [[[info objectForKey:@"properties"] objectForKey:@"mag"] floatValue];
            [lbl setText:[NSString stringWithFormat:@"%.1f Mw, %@", magnitude, time]];
            [lbl setFont:font];
            [cell addSubview:lbl];
        }];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MAP_OF_PLACE) {
        CGRect frame = self.view.bounds;
        frame.size.height = frame.size.height - 64.f;
        
        if (!CGRectEqualToRect(self.mapVC.mapView.frame, frame)) {
            [self.collectionView setScrollEnabled:NO];
            UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:MAP_OF_PLACE]];
            NSArray *coordinate = [[[[[UpdatedDataSource sharedUpdateDataSource] getGroup:self.selectedCity.row] objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"coordinates"];
            CLLocationDegrees latitude  = [[coordinate objectAtIndex:1] doubleValue];
            CLLocationDegrees longitude = [[coordinate objectAtIndex:0] doubleValue];
            CLLocationCoordinate2D coordinate2D = CLLocationCoordinate2DMake(latitude, longitude);
            [self.mapVC.mapView setRegion:MKCoordinateRegionMake(coordinate2D, MKCoordinateSpanMake(4.25, 4.25)) animated:YES];
            
            [UIView animateWithDuration:.25f animations:^{
                [self.collectionView bringSubviewToFront:cell];
                [cell setFrame:frame];
                [self.mapVC.mapView setFrame:frame];
            } completion:^(BOOL finished){
                [self.rightBarButtonItem setEnabled:YES];
            }];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize = CGSizeZero;
    switch (indexPath.section) {
        case NAME_OF_PLACE:
            cellSize = CGSizeMake(305, 60);
            break;
        case MAP_OF_PLACE:
            cellSize = CGSizeMake(305, 120);
            break;
        case EARTHQUAKE_PATH:
            cellSize = CGSizeMake(305, 17 * [self.mapVC.group count] + 10);
            break;
        default:
            break;
    }
    
    return cellSize;
}

#pragma mark - ADBannerViewDelegate
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    DLog(@"");
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    DLog(@"");
}

- (NSString*)getTimeInformation:(NSTimeInterval)time
{
    NSString *update = @"";
    NSTimeInterval d = time/1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:d];
    // assign date from string using formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // converts the date format as we wish
    [dateFormatter setDateFormat:@"MM/dd/yyyy 'at' hh:mm a"];
    update = [dateFormatter stringFromDate:date];
    
    return update;
}

@end
