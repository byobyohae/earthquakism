//
//  AppDelegate.m
//  EarthQuakism
//
//  Created by Yoon Lee on 3/30/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "AppDelegate.h"

static NSString *kCrashlyticsAPIKey         = @"ee8a2a12d8e01a8e22db2af464a3f44f3c12cbbe";
static NSString *kGoogleAnalysticAPIKey     = @"UA-51533831-1";
static NSString *DRAWER_IDENTIFIER          = @"DRAWER_IDENTIFIER";
static NSString *LEFT_IDENTIFIER            = @"LEFT_IDENTIFIER";
static NSString *CENTER_IDENTIFIER          = @"CENTER_IDENTIFIER";

@interface AppDelegate()
- (void)FBSessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error;
@end

@implementation AppDelegate
@synthesize window;
@synthesize drawerController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /* Crashlytics SDK */
    [Crashlytics startWithAPIKey:kCrashlyticsAPIKey];
    
    /* Google Analytics */
    [[GAI sharedInstance] setTrackUncaughtExceptions:YES];
    [[GAI sharedInstance] setDispatchInterval:20];
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] trackerWithTrackingId:kGoogleAnalysticAPIKey];
    
    /* FBSession */
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"] allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState status,
                                                          NSError *error) {
            [self FBSessionStateChanged:session state:status error:error];
        }];
    }
    
    // create window
    window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    // center
    RootViewController *rootVC = [[RootViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootVC];
    
    // left
    EQLeftPanelViewController *leftPanelVC = [[EQLeftPanelViewController alloc] init];
    
    // container
    drawerController = [[MMDrawerController alloc] initWithCenterViewController:navController leftDrawerViewController:leftPanelVC];
    
    // setup container
    [self.drawerController setMaximumLeftDrawerWidth:EQLeftDefaultWidth];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningCenterView];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModePanningCenterView];
    [self.drawerController setShouldStretchDrawer:NO];
    [self.drawerController setCenterHiddenInteractionMode:MMDrawerOpenCenterInteractionModeFull];
    
    // restoration identifier setup
    [leftPanelVC setRestorationIdentifier:LEFT_IDENTIFIER];
    [navController setRestorationIdentifier:CENTER_IDENTIFIER];
    [self.drawerController setRestorationIdentifier:DRAWER_IDENTIFIER];
    
    [self.window setRootViewController:self.drawerController];
    [self.window makeKeyAndVisible];
    
    // GoogleAnalytics: records class name
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:NSStringFromClass([self class])];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)FBSessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error
{
    /* error handler */
    if (error) {
        DLog(@".err FBSession %@", [error description]);
        NZAlertView *alert = [[NZAlertView alloc] init];
        NSString *titleStr      = EQLocalization(@"EQFacebookSessionTitleTextFailure");
        NSString *messageStr    = EQLocalization(@"EQFacebookSessionMessageTextFailure");
        
        [alert setAlertViewStyle:NZAlertStyleError];
        [alert setTextAlignment:NSTextAlignmentCenter];
        
        if ([FBErrorUtility userMessageForError:error]) {
            
        }
        else {
            switch ([FBErrorUtility errorCategoryForError:error]) {
                case FBErrorCategoryUserCancelled:
                case FBErrorCategoryAuthenticationReopenSession:
                default:
                    break;
            }
        }
        
        [alert setTitle:titleStr];
        [alert setMessage:messageStr];
        [alert show];
        
        // clears
        [FBSession.activeSession closeAndClearTokenInformation];
        return;
    }
    
    /* Session Handler */
    switch (state) {
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            /* Logout */
            break;
        case FBSessionStateOpen:
            /* Login */
            break;
        default:
            break;
    }
}

@end
