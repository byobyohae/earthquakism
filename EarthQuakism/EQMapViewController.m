//
//  EQMapViewController.m
//  EarthQuakism
//
//  Created by Yoon Lee on 7/10/14.
//  Copyright (c) 2014  Yoon Lee. All rights reserved.
//

#import "EQMapViewController.h"
#import "EQMKAnnotation.h"
#import "UIImage+Category.h"
@interface EQMapViewController ()
@property(nonatomic, strong)ADBannerView *adView;
@end

@implementation EQMapViewController
@synthesize group = _group;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view.layer setBorderColor:CDGREY().CGColor];
    [self.view.layer setBorderWidth:0.5f];
    [self setTitle:@"Maps"];
    
    /* mapview settings */
    _mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    [_mapView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.mapView setShowsBuildings:YES];
    [self.mapView setShowsPointsOfInterest:YES];
    [self.mapView setExclusiveTouch:YES];
    
    /* mapview coordinate settings */
    NSArray *coordinate = [[[self.group objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"coordinates"];
    CLLocationDegrees latitude  = [[coordinate objectAtIndex:1] doubleValue];
    CLLocationDegrees longitude = [[coordinate objectAtIndex:0] doubleValue];
    CLLocationCoordinate2D coordinate2D = CLLocationCoordinate2DMake(latitude, longitude);
    NSString *place = [[[self.group objectAtIndex:0] objectForKey:@"properties"] objectForKey:@"place"];
    NSArray *originInfo = [place componentsSeparatedByString:@" of "];
    if (originInfo.count > 1)
        place = [originInfo objectAtIndex:1];
    NSString *magnitude = [NSString stringWithFormat:@"%.1f Mw", [[[[self.group objectAtIndex:0] objectForKey:@"properties"] objectForKey:@"mag"] floatValue]];
    EQMKAnnotation *annotation = [[EQMKAnnotation alloc] initWithTitle:place withSubTitle:magnitude andCoordinate:coordinate2D];
    [self.mapView addAnnotation:annotation];
    
    UIButton *originEQ = [UIButton buttonWithType:UIButtonTypeCustom];
    [originEQ setBackgroundImage:[UIImage imageWithColor:CDBLUE()] forState:UIControlStateNormal];
    [originEQ addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [originEQ setFrame:CGRectMake(5, 440, 40, 40)];
    [originEQ.layer setCornerRadius:4.5f];
    [originEQ.layer setMasksToBounds:YES];
    [self.mapView addSubview:originEQ];
    [self.view addSubview:self.mapView];
    
    /* advertising! let's fired up! */
    _adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    [self.adView setDelegate:self];
    [self.view addSubview:self.adView];
    
    /* GoogleAnalytics: records class name */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:NSStringFromClass([self class])];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    /* GoogleAnalytics: records class name */
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"PLACE_EARTHQUAKE_PLACE"
                                                          action:@"PLACE_EARTHQUAKE_PLACE_MAP_LOOK_UP"
                                                           label:place
                                                           value:nil] build]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect frame = self.view.bounds;
    frame.size.height -= self.adView.frame.size.height;
    [self.mapView setFrame:frame];
    
    frame = self.adView.frame;
    frame.origin.y = self.view.frame.size.height - self.adView.frame.size.height;
    [self.adView setFrame:frame];
    
    NSArray *aEQRegions = [[UpdatedDataSource sharedUpdateDataSource] getEntireEQRegions];
    [aEQRegions enumerateObjectsUsingBlock:^(NSArray *content, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = [content objectAtIndex:0];
        NSLog(@"%@", [[dict objectForKey:@"geometry"] objectForKey:@"coordinates"]);
    }];
}

- (void)buttonClicked:(UIButton *)button
{
    NSLog(@".point me");
    EQMKAnnotation *annotation = [self.mapView.annotations objectAtIndex:0];
    CLLocationCoordinate2D coordinate2D = annotation.coordinate;
    [self.mapView setCenterCoordinate:coordinate2D animated:YES];
}

#pragma mark ADBannerViewDelegate
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    DLog(@"");
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    DLog(@"");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
