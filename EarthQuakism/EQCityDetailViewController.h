//
//  EQCityDetailViewController.h
//  EarthQuakism
//
//  Created by Yoon Lee on 4/14/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EQCityDetailViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,
                                                          UICollectionViewDelegateFlowLayout, ADBannerViewDelegate>

@property(nonatomic, weak)IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong)NSIndexPath *selectedCity;

@end
