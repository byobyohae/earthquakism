//
//  RootViewController.m
//  EarthQuakism
//
//  Created by Yoon Lee on 3/30/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "RootViewController.h"
#import "EQCityDetailViewController.h"
#import "NZAlertView.h"
#import "EQMapViewController.h"

@interface RootViewController()
@property(nonatomic, strong)UITableView *hazardTableView;
@property(nonatomic, strong)NSMutableArray *hazardsInfo;

@property(nonatomic, strong)UIRefreshControl *refControl;
@property(nonatomic, strong)ADBannerView *adView;

- (void)rightBarButtonClicked:(UIBarButtonItem *)rightBarButton;
@end

@implementation RootViewController
@synthesize hazardsInfo;
@synthesize hazardTableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:CWHITE()];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:[UIFont fontWithName:@"Avenir-Medium" size:20] forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:CBLACK() forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTitleTextAttributes:navBarTextAttributes];

    /* query manager */
    EQQueryManager *query = [[EQQueryManager alloc] init];
    [query setDelegate:self];
    [query shakeEQ:NULL];
    [query executeQuery];
    query = nil;
    
    /* earthquake tableview */
    hazardTableView= [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [hazardTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.hazardTableView setDelegate:self];
    [self.hazardTableView setDataSource:self];
    
    /* right bar button mapview */
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Maps" style:UIBarButtonItemStyleBordered target:self action:@selector(rightBarButtonClicked:)];
    [self.navigationItem setRightBarButtonItem:rightBarButtonItem];
    
    /* refresh query */
    _refControl = [[UIRefreshControl alloc] init];
    [_refControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.hazardTableView addSubview:_refControl];
    [self.hazardTableView setRowHeight:50];
    [self.view addSubview:self.hazardTableView];
    
    /* advertising! let's fired up! */
    _adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    [self.adView setDelegate:self];
    [self.view addSubview:self.adView];

    /* GoogleAnalytics: records class name */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:NSStringFromClass([self class])];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect frame = self.view.bounds;
    frame.size.height -= self.adView.frame.size.height;
    [hazardTableView setFrame:frame];
    [self setTitle:@"Earthquake"];
    
    frame = self.adView.frame;
    frame.origin.y = self.view.frame.size.height - self.adView.frame.size.height;
    [self.adView setFrame:frame];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self setTitle:@""];
}

- (void)rightBarButtonClicked:(UIBarButtonItem *)rightBarButton
{
    EQMapViewController *mapVC = [[EQMapViewController alloc] init];
    [self.navigationController pushViewController:mapVC animated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [hazardsInfo count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIFont *font = [UIFont fontWithName:@"Helvetica Neue" size:17];
    UIFont *detailFont = [UIFont fontWithName:@"Avenir-Medium" size:13];
    NSArray *group = [[UpdatedDataSource sharedUpdateDataSource] getGroup:indexPath.row];
    NSDictionary *info = [group objectAtIndex:0];
    NSString *place = [[info objectForKey:@"properties"] objectForKey:@"place"];
    
    NSArray *originInfo = [place componentsSeparatedByString:@" of "];
    if (originInfo.count > 1)
        place = [[originInfo objectAtIndex:1] capitalizedString];
    
    cell.accessoryView = nil;
    
    if (group.count > 1) {
        UIImageView *sequence = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ArrowAccessory.png"]];
        [cell setAccessoryView:sequence];
        place = [NSString stringWithFormat:@"%@ (%lu)", place, (unsigned long)group.count];
    }
    
    [cell.textLabel setText:place];
    [cell.textLabel setFont:font];
    
    [cell.detailTextLabel setFont:detailFont];
    [cell.detailTextLabel setTextColor:CDGREY()];
    
    NSString *time = [self getTimeInformation:[[[info objectForKey:@"properties"] objectForKey:@"time"] doubleValue]];

    // !!!need to fix this part!!!
    bool isTsunami = [NSNull null] != [[info objectForKey:@"properties"] objectForKey:@"tsunami"];
    
    if (isTsunami)
        [cell.detailTextLabel setTextColor:CDRED()];
    NSString *mag = [NSString stringWithFormat:@"%.1f Mw, %@", [[[info objectForKey:@"properties"] objectForKey:@"mag"] floatValue], time];
    [cell.detailTextLabel setText:mag];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    EQCityDetailViewController *detailView = [[EQCityDetailViewController alloc] initWithNibName:@"EQCityDetailViewController" bundle:nil];
    [detailView setSelectedCity:indexPath];
    [self.navigationController pushViewController:detailView animated:YES];
}

#pragma mark ADBannerViewDelegate
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    DLog(@"");
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    DLog(@"");
}

#pragma mark QueryManagerDelegate
- (void)didReceivedResponseFromQueryManager:(EQQueryManager *)qm withResponse:(id)response
{
    const NSString *property        = @"properties";
    const NSString *searchKeyword   = @"place";
    
    NSArray *features = [response objectForKey:@"features"];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if ([features count]) {
        NSMutableDictionary *dup = [[NSMutableDictionary alloc] init];

        [features enumerateObjectsUsingBlock:^(NSDictionary *live, NSUInteger idx, BOOL *stop) {
            NSString *key = [[live objectForKey:property] objectForKey:searchKeyword];
            NSArray *originInfo = [key componentsSeparatedByString:@" of "];
            if (originInfo.count > 1)
                key = [[originInfo objectAtIndex:1] capitalizedString];
            [dup setObject:[NSNull null] forKey:key];
        }];
        
        NSMutableArray *dupList = [[NSMutableArray alloc] init];
        [features enumerateObjectsUsingBlock:^(NSDictionary *live, NSUInteger idx, BOOL *stop) {
            NSString *city = [[live objectForKey:property] objectForKey:searchKeyword];
            NSArray *originInfo = [city componentsSeparatedByString:@" of "];
            if (originInfo.count > 1)
                city = [[originInfo objectAtIndex:1] capitalizedString];
            
            id cities = [dup objectForKey:city];
            // if it is exist
            if (![cities isEqual:[NSNull null]]) {
                NSMutableArray *cities = [dup objectForKey:city];
                [cities addObject:live];
            }
            else {
                // create array
                // add city
                // set to map
                // add to duplist
                NSMutableArray *cities = [[NSMutableArray alloc] init];
                [cities addObject:live];
                [dup setObject:cities forKey:city];
                [dupList addObject:cities];
            }
        }];
        
        [self setHazardsInfo:dupList];
        [[UpdatedDataSource sharedUpdateDataSource] setMergedBySameCity:dupList];
        
        [_refControl endRefreshing];
        [self.hazardTableView reloadData];
    }
}

- (NSString*)getTimeInformation:(NSTimeInterval)time
{
    NSString *update = @"";
    NSTimeInterval d = time/1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:d];
    // assign date from string using formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // converts the date format as we wish
    [dateFormatter setDateFormat:@"MM/dd/yyyy 'at' hh:mm a"];
    update = [dateFormatter stringFromDate:date];
    
    return update;
}

- (void)refresh:(UIRefreshControl*)control
{
    EQQueryManager *qManager = [[EQQueryManager alloc] init];
    [qManager setDelegate:self];
    [qManager shakeEQ:NULL];
    [qManager executeQuery];
    qManager = nil;
    
    // GoogleAnalytics Action
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"REFRESH_EARTHQUAKE"
                                                          action:@"REFRESH_EARTHQUAKE_ACTION"
                                                           label:@"EARTHQUAKE_DATA_REQUEST"
                                                           value:nil] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
