//
//  RootViewController.h
//  EarthQuakism
//
//  Created by Yoon Lee on 3/30/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EQQueryManager.h"

@interface RootViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,
                                                 ADBannerViewDelegate, QueryManagerDelegate>

@end
