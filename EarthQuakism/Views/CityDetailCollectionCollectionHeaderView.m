//
//  CityDetailCollectionCollectionHeaderView.m
//  EarthQuakism
//
//  Created by Yoon Lee on 4/15/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "CityDetailCollectionCollectionHeaderView.h"

@interface CityDetailCollectionCollectionHeaderView()
@property(nonatomic, retain)UILabel *titleLabel;
@end

@implementation CityDetailCollectionCollectionHeaderView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        frame = CGRectMake(15, 12.5, 270, 22);
        UIFont *font = [UIFont fontWithName:@"Avenir-Medium" size:20];
        _titleLabel = [[UILabel alloc] initWithFrame:frame];
        [_titleLabel setFont:font];
        [self addSubview:_titleLabel];
        
        CALayer *lined = [CALayer layer];
        [lined setFrame:CGRectMake(10, 40, 300, 0.5)];
        [lined setBackgroundColor:CDGREY().CGColor];
        [self.layer addSublayer:lined];
    }
    
    return self;
}

- (void)setTitle:(NSString *)title
{
    [_titleLabel setText:title];
}

@end
