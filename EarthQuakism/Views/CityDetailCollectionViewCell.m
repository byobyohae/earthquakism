//
//  CityDetailCollectionViewCell.m
//  EarthQuakism
//
//  Created by Yoon Lee on 4/21/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "CityDetailCollectionViewCell.h"

@implementation CityDetailCollectionViewCell


- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _textLabel = [[UILabel alloc] initWithFrame:self.contentView.bounds];
        UIFont *font = [UIFont fontWithName:@"Avenir-Medium" size:20];
        [self.textLabel setFont:font];
        [self.textLabel setTextAlignment:NSTextAlignmentCenter];
        [self.contentView addSubview:self.textLabel];
    }
    
    return self;
}

@end
