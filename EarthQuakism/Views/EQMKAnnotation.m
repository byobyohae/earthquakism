//
//  EQMKAnnotation.m
//  EarthQuakism
//
//  Created by Yoon Lee on 4/1/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import "EQMKAnnotation.h"

@implementation EQMKAnnotation
@synthesize title, subtitle;
@synthesize coordinate;

- (id)initWithTitle:(NSString*)ttl withSubTitle:(NSString*)subttl andCoordinate:(CLLocationCoordinate2D)c2d
{
    if ([super init]) {
        self.title = ttl;
        self.subtitle = subttl;
        self.coordinate = c2d;
    }
    
    return self;
}

@end
