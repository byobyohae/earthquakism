//
//  CityDetailCollectionViewCell.h
//  EarthQuakism
//
//  Created by Yoon Lee on 4/21/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityDetailCollectionViewCell : UICollectionViewCell

@property(nonatomic, strong)UILabel *textLabel;

@end
