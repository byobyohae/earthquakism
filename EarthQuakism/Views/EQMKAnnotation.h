//
//  EQMKAnnotation.h
//  EarthQuakism
//
//  Created by Yoon Lee on 4/1/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface EQMKAnnotation : NSObject<MKAnnotation>
{
    NSString *title;
    NSString *subtitle;
	CLLocationCoordinate2D coordinate;
}
@property (nonatomic, copy) NSString *title, *subtitle;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString*)ttl withSubTitle:(NSString*)subttl andCoordinate:(CLLocationCoordinate2D)c2d;

@end
