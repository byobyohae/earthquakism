//
//  CityDetailCollectionCollectionHeaderView.h
//  EarthQuakism
//
//  Created by Yoon Lee on 4/15/14.
//  Copyright (c) 2014 Yoon Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityDetailCollectionCollectionHeaderView : UICollectionReusableView
@property(nonatomic, copy)NSString *title;
@end
