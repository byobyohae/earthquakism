//
//  EQMapViewController.h
//  EarthQuakism
//
//  Created by Yoon Lee on 7/10/14.
//  Copyright (c) 2014  Yoon Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>

@interface EQMapViewController : UIViewController <UIGestureRecognizerDelegate, ADBannerViewDelegate>

@property(nonatomic, strong)MKMapView *mapView;
@property(nonatomic, strong)NSArray *group;
@end
